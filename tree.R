#HW 07/12/2017
#First Git project
# Load packages ----
install.packages("ggplot2")
library(ggplot2)

#set working directory
setwd("C:/Users/ellie/Documents/tlug_away_day_2017_12_07")

# Read data ----
dat1 <- read.csv("tree_left.csv")
dat2 <- read.csv("tree_right.csv")

# Make plot ----
ggplot() +
	geom_bar(data = dat1, aes(x=x1, y=x2),stat = "identity", fill = '#31a354') +
	geom_bar(data = dat2, aes(x=x1, y=x2),stat = "identity", fill = '#31a354') +
	geom_point(data = dat1,aes(x = xvar, y = yvar, size = siz, colour = as.factor(col)) ) +
	geom_point(data = dat2,aes(x = xvar, y = yvar, size = siz, colour = as.factor(col)) ) +
	coord_flip() + 
	theme_void() + 
	theme(legend.position = "none")
